package com.example.demo_security.Utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EntryPasswordUtils {

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        System.out.printf(encoder.encode("user"));
    }
}
