package com.example.demo_security.service;

import com.example.demo_security.model.dto.UserDTO;

public interface UserService {

    UserDTO findUserByUserName(String userName);
}
