package com.example.demo_security.model.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
public class UserDTO {

    private String username;

    private String entryPassword;

    private boolean enable;

    private Long roleId;

    private String roleName;
}
