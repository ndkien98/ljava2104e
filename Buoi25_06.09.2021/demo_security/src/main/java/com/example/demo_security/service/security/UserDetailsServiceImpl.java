package com.example.demo_security.service.security;

import com.example.demo_security.model.entity.RoleEntity;
import com.example.demo_security.model.entity.UserEntity;
import com.example.demo_security.repositories.RoleRepository;
import com.example.demo_security.repositories.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserDetailsServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity userEntity = userRepository.findByUsername(username);

        if (userEntity == null){
            System.out.printf("user not found!");
            throw new UsernameNotFoundException("user " + username + "not found");
        }

        RoleEntity role = roleRepository.findByRoleId(userEntity.getRoleId());

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        if (role != null){
            GrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
            grantedAuthorities.add(authority);
        }

        UserDetails userDetails = new User(userEntity.getUsername(), userEntity.getEntryPassword(), grantedAuthorities);

        return userDetails;
    }
}
