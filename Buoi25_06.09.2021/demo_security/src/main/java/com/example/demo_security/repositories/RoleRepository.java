package com.example.demo_security.repositories;

import com.example.demo_security.model.entity.RoleEntity;
import com.example.demo_security.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity,Long> {

    RoleEntity findByRoleId(Long roleId);
}
