package com.example.demo_security.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;

    @Column(name = "username",length = 36)
    private String username;

    private String entryPassword;

    private boolean enable;

    private Long roleId;

}
