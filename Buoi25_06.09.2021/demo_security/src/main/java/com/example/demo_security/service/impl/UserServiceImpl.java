package com.example.demo_security.service.impl;

import com.example.demo_security.model.dto.UserDTO;
import com.example.demo_security.repositories.RoleRepository;
import com.example.demo_security.repositories.UserRepository;
import com.example.demo_security.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    private final ModelMapper modelMapper;

    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository repository, ModelMapper modelMapper, RoleRepository roleRepository) {
        this.repository = repository;
        this.modelMapper = modelMapper;
        this.roleRepository = roleRepository;
    }

    public UserDTO findUserByUserName(String userName){
        UserDTO userDTO = modelMapper.map(repository.findByUsername(userName),UserDTO.class);
        userDTO.setRoleName(roleRepository.findByRoleId(userDTO.getRoleId()).getRoleName());
        return userDTO;
    }
}
