package com.example.demo_security.controller;

import com.example.demo_security.model.dto.UserDTO;
import com.example.demo_security.model.entity.UserEntity;
import com.example.demo_security.service.UserService;
import com.example.demo_security.service.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/","welcome"},method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("title","welcome");
        model.addAttribute("message","welcome to page");
        return "welcomePage";
    }

    @RequestMapping(value = "/admin",method = RequestMethod.GET)
    public String adminPage(Model model){

        UserDetails userContext = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        UserDTO userDTO = userService.findUserByUserName(userContext.getUsername());
        if (userDTO.getRoleName().equals("ROLE_ADMIN")){
            model.addAttribute("userInfo",userDTO);
            return "adminPage";
        }
        return "/403Page";
    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String loginPage(Model model){
        return "loginPage";
    }


    @RequestMapping(value = "/logoutSuccessful",method = RequestMethod.GET)
    public String logout(Model model){
        model.addAttribute("title","logout");
        return "logoutSuccessfulPage";
    }

    @RequestMapping(value = "/userInfo",method = RequestMethod.GET)
    public String userInfo(Model model, Principal principal){

        String username = principal.getName();

        UserDTO userDTO = userService.findUserByUserName(username);
        model.addAttribute("userInfo",userDTO);
        return "userInfoPage";
    }

    @RequestMapping(value = "/403",method = RequestMethod.GET)
    public String accessDenied(Model model,Principal principal){
        return "/403Page";
    }

}
