package com.example.buoi24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Buoi24Application {

	public static void main(String[] args) {
		SpringApplication.run(Buoi24Application.class, args);
	}

}
