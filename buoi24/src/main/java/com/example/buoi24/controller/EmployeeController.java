package com.example.buoi24.controller;

import com.example.buoi24.model.dto.EmployeeDTO;
import com.example.buoi24.model.request.EmployeeRequest;
import com.example.buoi24.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping()
    public ResponseEntity<List<EmployeeDTO>> getAll() {
        return ResponseEntity.ok(employeeService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeDTO> findById(@PathVariable Integer id){
        return ResponseEntity.ok(employeeService.findById(id));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity findByName(@PathVariable String name){
        return ResponseEntity.ok(employeeService.findByName(name));
    }

    @PostMapping()
    public ResponseEntity<List<EmployeeDTO>> addEmployee(@RequestBody EmployeeRequest employeeRequest){
        return ResponseEntity.ok(employeeService.addEmployee(employeeRequest));
    }
}
