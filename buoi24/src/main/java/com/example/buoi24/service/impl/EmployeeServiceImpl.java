package com.example.buoi24.service.impl;

import com.example.buoi24.model.entity.EmployeeEntity;
import com.example.buoi24.model.dto.EmployeeDTO;
import com.example.buoi24.model.request.EmployeeRequest;
import com.example.buoi24.repositories.EmployeeRepository;
import com.example.buoi24.service.EmployeeService;
import com.example.buoi24.util.mapper.EmployeeMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<EmployeeDTO> getAll(){
        List<EmployeeEntity> employeeEntities = employeeRepository.findAll();
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        for (EmployeeEntity employee:employeeEntities
             ) {
            EmployeeDTO employeeDTO = new EmployeeDTO();
            employeeDTO.setId(employee.getId());
            employeeDTO.setAge(employee.getAge());
            employeeDTO.setName(employee.getName());
            employeeDTOS.add(employeeDTO);
        }

        return employeeDTOS;
    }

    public List<EmployeeDTO> findByName(String name){
        List<EmployeeEntity> employeeEntity = employeeRepository.findByName("%" + name + "%");
        return EmployeeMapper.convertEntitiesToDTOs(employeeEntity);
    }

    public EmployeeDTO findById(Integer id){
        EmployeeEntity employeeEntity = employeeRepository.findById(id).get();

        EmployeeDTO employeeDTO = modelMapper.map(employeeEntity,EmployeeDTO.class);

        return employeeDTO;
    }

    public List<EmployeeDTO> addEmployee(EmployeeRequest employeeRequest){
        EmployeeEntity employeeEntity = modelMapper.map(employeeRequest,EmployeeEntity.class);
        employeeRepository.save(employeeEntity);
        List<EmployeeDTO> employeeDTOS = this.getAll();
        return employeeDTOS;
    }


}
