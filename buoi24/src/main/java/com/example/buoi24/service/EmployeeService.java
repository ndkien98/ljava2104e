package com.example.buoi24.service;


import com.example.buoi24.model.dto.EmployeeDTO;
import com.example.buoi24.model.request.EmployeeRequest;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDTO> getAll();

    EmployeeDTO findById(Integer id);

    List<EmployeeDTO> findByName(String name);

    List<EmployeeDTO> addEmployee(EmployeeRequest employeeRequest);
}
