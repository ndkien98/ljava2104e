package com.example.buoi24.repositories;

import com.example.buoi24.model.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity,Integer> {

    @Query("SELECT e FROM EmployeeEntity e WHERE e.name LIKE :name")
    List<EmployeeEntity> findByName(@Param("name") String name);

    @Query(value = "SELECT * FROM employee WHERE employee.age = :age",nativeQuery = true)
    EmployeeEntity findByAge(@Param("age") Integer age);
}
