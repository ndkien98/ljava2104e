package com.example.buoi24.util.mapper;

import com.example.buoi24.model.dto.EmployeeDTO;
import com.example.buoi24.model.entity.EmployeeEntity;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeMapper {

    public static List<EmployeeDTO> convertEntitiesToDTOs(List<EmployeeEntity> employeeEntities){
        ModelMapper modelMapper = new ModelMapper();
        return employeeEntities.stream().map(entity -> modelMapper.map(entity,EmployeeDTO.class)).collect(Collectors.toList());
    }
}
